﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdayBotLib.Wrappers
{
    public struct DiscordUserId
    {
        public ulong Value { get; set; }

        public DiscordUserId(ulong userId)
        {
            Value = userId;
        }

        public static implicit operator DiscordUserId(ulong userId)
        {
            return new DiscordUserId(userId);
        }
    }
}
