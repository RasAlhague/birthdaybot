﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdayBotLib.Wrappers
{
    public struct GuildId
    {
        public ulong Value { get; set; }

        public GuildId(ulong guildId)
        {
            Value = guildId;
        }

        public static implicit operator GuildId(ulong guildId)
        {
            return new GuildId(guildId);
        }
    }
}
