﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdayBotLib.Models
{
    public interface IBirthdayNotification
    {
        int Id { get; set; }
        ulong NotifyUserId { get; set; }
        ulong GuildId { get; set; }
        int BirthdayId { get; set; }
        BirthdayState State { get; set; }
        int LastNotified { get; set; }
        DateTime CreateDate { get; set; }
        DateTime? ModifyDate { get; set; }
        ulong CreateUserId { get; set; }
        ulong? ModifyUserId { get; set; }
    }
}
