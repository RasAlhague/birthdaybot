﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdayBotLib.Models
{
    public interface IBirthday
    {
        int Id { get; set; }
        ulong BirthdayUserId { get; set; }
        ulong GuildId { get; set; }
        DateTime Date { get; set; }
        BirthdayState State { get; set; }
        DateTime CreateDate { get; set; }
        DateTime? ModifyDate { get; set; }
        ulong CreateUserId { get; set; }
        ulong? ModifyUserId { get; set; }
    }
}
