﻿using BirthdayBotLib.Models;
using BirthdayBotLib.Services;
using DSharpPlus;
using DSharpPlus.Entities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdayBotLib
{
    public class BirthdayNotifier<TBirthday, TBirthdayNotification>
        where TBirthday : IBirthday
        where TBirthdayNotification : IBirthdayNotification
    {
        private readonly IBirthdayService<TBirthday> _birthdayService;
        private readonly INotificationService<TBirthdayNotification> _notificationService;
        private readonly IDbConnectionService _connectionService;

        public BirthdayNotifier(IBirthdayService<TBirthday> birthdayService, INotificationService<TBirthdayNotification> notificationService, IDbConnectionService connectionService)
        {
            _birthdayService = birthdayService;
            _notificationService = notificationService;
            _connectionService = connectionService;
        }

        public async Task RunNotifierAsync(DiscordClient discordClient, TimeSpan interval)
        {
            while (true)
            {
                await Task.Delay(interval);
                discordClient.Logger.LogInformation("Notifying users for birthdays!");

                if (discordClient == null)
                {
                    return;
                }

                using var conn = _connectionService.CreateDbConnection();

                foreach (var guild in discordClient.Guilds)
                {
                    await NotifyGuildAsync(discordClient, guild.Value, conn);
                }
            }
        }

        private async Task NotifyGuildAsync(DiscordClient discordClient, DiscordGuild guild, IDbConnection conn)
        {
            var birthdays = await _birthdayService.GetForGuildAsync(conn, guild.Id);

            foreach (var birthday in birthdays)
            {
                if (birthday.Date.Day == DateTime.UtcNow.Day && birthday.Date.Month == DateTime.UtcNow.Month)
                {
                    try
                    {
                        var notifications = await _notificationService.GetAllForBirthdayAsync(conn, birthday.Id);
                        var birthdayUser = await guild.GetMemberAsync(birthday.BirthdayUserId);

                        foreach (var notification in notifications)
                        {
                            await NotifyUserAsync(discordClient, guild, birthday, birthdayUser, notification, conn);        
                        }

                        // await guild.GetDefaultChannel().SendMessageAsync($"Happy birthday {birthdayUser.Mention}. Sorry if this is a bit early or late, this message comes on UTC 0 time.");
                    }
                    catch (Exception ex)
                    {
                        discordClient.Logger.LogError(ex, "Error while notifing users about birthdays!");
                    }
                }
            }              
        }

        private async Task NotifyUserAsync(DiscordClient discordClient, DiscordGuild guild, TBirthday birthday, DiscordMember birthdayUser, IBirthdayNotification notification, IDbConnection conn)
        {
            try
            {
                if (notification.LastNotified >= DateTime.UtcNow.Year)
                {
                    return;
                }

                var notifyUser = await guild.GetMemberAsync(notification.NotifyUserId);
                var dmChannel = await notifyUser.CreateDmChannelAsync();

                await dmChannel.SendMessageAsync($"The user `{birthdayUser.Username}#{birthdayUser.Discriminator}` has birthday. The exact date is `{birthday.Date.ToShortDateString()}`. Wish him/her/them a happy birthday.");

                notification.LastNotified = DateTime.UtcNow.Year;
                notification.ModifyDate = DateTime.UtcNow;
                await _notificationService.UpdateAsync(conn, (TBirthdayNotification)notification);
            }
            catch (Exception ex)
            {
                discordClient.Logger.LogError(ex, "Error while notifing users about birthdays!");
            }
        }
    }
}
