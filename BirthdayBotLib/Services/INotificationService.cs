﻿using BirthdayBotLib.Models;
using BirthdayBotLib.Wrappers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdayBotLib.Services
{
    public interface INotificationService<T> where T : IBirthdayNotification
    {
        Task<IBirthdayNotification> CreateAsync(IDbConnection conn, T notification);
        Task<IBirthdayNotification> UpdateAsync(IDbConnection conn, T notification);
        Task<IEnumerable<IBirthdayNotification>> GetAllForUserAsync(IDbConnection conn, GuildId guildId, DiscordUserId notifyUserId);
        Task<IEnumerable<IBirthdayNotification>> GetAllForBirthdayAsync(IDbConnection conn, int birthdayId);
        Task<T> GetAsync(IDbConnection conn, GuildId guildId, DiscordUserId notifyUserId, int birthdayId);
        Task DeleteAsync(IDbConnection conn, T notification);
    }
}
