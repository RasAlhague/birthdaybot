﻿using BirthdayBotLib.Models;
using BirthdayBotLib.Wrappers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdayBotLib.Services
{
    public interface IBirthdayService<T> where T : IBirthday 
    {
        Task<T> CreateAsync(IDbConnection conn, GuildId guildId, DiscordUserId birthdayUserId, DateTime birthDate, BirthdayState state);
        Task<T> UpdateAsync(IDbConnection conn, T birthdayModel);
        Task<IEnumerable<T>> GetForGuildAsync(IDbConnection conn, GuildId guildId);
        Task<T> GetAsync(IDbConnection conn, GuildId guildId, DiscordUserId userId);
        Task<T> GetAsync(IDbConnection conn, int birthdayId);
        Task DeleteAsync(IDbConnection conn, T birthdayModel);
    }
}
