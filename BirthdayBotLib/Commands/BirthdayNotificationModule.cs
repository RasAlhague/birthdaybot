﻿using BirthdayBotLib.Models;
using BirthdayBotLib.Services;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdayBotLib.Commands
{
    [SlashCommandGroup("notifications", "Commands for birthday notifications.")]
    public class BirthdayNotificationModule<TBirthday, TNotification> : SlashCommandModule
        where TBirthday : IBirthday, new()
        where TNotification : IBirthdayNotification, new()
    {
        private readonly IBirthdayService<TBirthday> _birthdayService;
        private readonly INotificationService<TNotification> _notificationService;
        private readonly IDbConnectionService _connectionService;

        public BirthdayNotificationModule(IBirthdayService<TBirthday> birthdayService, INotificationService<TNotification> notificationService, IDbConnectionService dbConnectionService)
        {
            _birthdayService = birthdayService;
            _notificationService = notificationService;
            _connectionService = dbConnectionService;
        }


        [SlashCommand("show", "Shows a list of all notifications you have active.")]
        public async Task Show(InteractionContext ctx)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            using var conn = _connectionService.CreateDbConnection();

            var notifications = await _notificationService.GetAllForUserAsync(conn, ctx.Guild.Id, ctx.User.Id);

            if (!notifications.Any())
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("You dont have any notifications on this guild."));
                return;
            }

            var embedBuilder = new DiscordEmbedBuilder()
                .WithTitle("Birthday Notifications:")
                .WithDescription("To delete notifications use `/birthday notify delete`.")
                .WithAuthor(ctx.User.Username, null, ctx.User.AvatarUrl);

            foreach (var notification in notifications)
            {
                var birthday = await _birthdayService.GetAsync(conn, notification.BirthdayId);
                var user = await ctx.Guild.GetMemberAsync(birthday.BirthdayUserId);

                embedBuilder.AddField($"Birthday {notification.BirthdayId}", $"{user.Username}#{user.Discriminator}");
            }

            await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(embedBuilder));
        }

        [SlashCommand("set", "Sets a notification for the birthdate of a user.")]
        public async Task Set(InteractionContext ctx, [Option("user", "The user whos birthday you want to be notified for.")] DiscordUser discordUser)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            using var conn = _connectionService.CreateDbConnection();

            try
            {
                var birthday = await _birthdayService.GetAsync(conn, ctx.Guild.Id, discordUser.Id);

                if (birthday is null)
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("The specified user does not have his birthday set."));
                    return;
                }

                var notification = await _notificationService.GetAsync(conn, ctx.Guild.Id, ctx.User.Id, birthday.Id);

                if (notification is null)
                {
                    notification = new TNotification
                    {
                        NotifyUserId = ctx.User.Id,
                        GuildId = ctx.Guild.Id,
                        BirthdayId = birthday.Id,
                        State = BirthdayState.Active,
                        CreateDate = DateTime.UtcNow,
                        CreateUserId = ctx.User.Id,
                    };

                    await _notificationService.CreateAsync(conn, notification);
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("A new notification has been set up!"));
                }
                else
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Notification already set up for this user!"));
                }
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Error while something bla bla bla");
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("An error occured during execution"));
            }
        }

        [SlashCommand("reset", "Resets a notification for the birthdate of a user.")]
        public async Task Delete(InteractionContext ctx, [Option("user", "The user whos birthday you dont want to be notified for.")] DiscordUser discordUser)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            using var conn = _connectionService.CreateDbConnection();

            try
            {
                var birthday = await _birthdayService.GetAsync(conn, ctx.Guild.Id, discordUser.Id);

                if (birthday is null)
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("The specified user does not have his birthday set."));
                    return;
                }

                var notification = await _notificationService.GetAsync(conn, ctx.Guild.Id, ctx.User.Id, birthday.Id);

                if (notification is null)
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("You have no notification for the specified user set."));
                }
                else
                {
                    await _notificationService.DeleteAsync(conn, notification);
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("The notification has been reset."));
                }
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Error while something bla bla bla");
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("An error occured during execution"));
            }
            
        }
    }
}
