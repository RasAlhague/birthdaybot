﻿using BirthdayBotLib.Models;
using BirthdayBotLib.Services;
using Dapper;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdayBotLib.Commands
{
    [SlashCommandGroup("birthday", "Commands for birthday notifications.")]
    public class BirthdayCommandModule<TBirthday, TNotification> : SlashCommandModule
        where TBirthday : IBirthday
        where TNotification : IBirthdayNotification
    {
        private readonly IBirthdayService<TBirthday> _birthdayService;
        private readonly INotificationService<TNotification> _notificationService;
        private readonly IDbConnectionService _connectionService;

        public BirthdayCommandModule(IBirthdayService<TBirthday> birthdayService, INotificationService<TNotification> notificationService, IDbConnectionService dbConnectionService)
        {
            _birthdayService = birthdayService;
            _notificationService = notificationService;
            _connectionService = dbConnectionService;
        }

        [SlashCommand("show", "Shows the current birthdate you have set.")]
        public async Task Show(InteractionContext ctx)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            using var conn = _connectionService.CreateDbConnection();

            var birthday = await _birthdayService.GetAsync(conn, ctx.Guild.Id, ctx.User.Id);

            if (birthday is null)
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("You dont have a birthday set!"));
                return;
            }

            var embedBuilder = new DiscordEmbedBuilder()
                    .WithTitle("Birthdate:")
                    .WithDescription(birthday.Date.ToShortDateString())
                    .WithAuthor(ctx.User.Username, null, ctx.User.AvatarUrl);

            await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(embedBuilder));
        }

        [SlashCommand("set", "Sets the birthdate of the user of the command.")]
        public async Task Set(InteractionContext ctx, [Option("birthday", "The birthday of a user. Use 'DD.MM.YYYY' as format for it.")] string birthday)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            using var conn = _connectionService.CreateDbConnection();

            if (!DateTime.TryParseExact(birthday, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime birthdate))
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("This is not a valid date!"));
                return;
            }

            try
            {
                TBirthday dbBirthday = await _birthdayService.GetAsync(conn, ctx.Guild.Id, ctx.User.Id);

                if (dbBirthday is null)
                {
                    dbBirthday = await _birthdayService.CreateAsync(conn, ctx.Guild.Id, ctx.User.Id, birthdate, BirthdayState.Active);

                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Your birthday has been set!"));
                }
                else
                {
                    dbBirthday.Date = birthdate;
                    dbBirthday.ModifyDate = DateTime.UtcNow;
                    dbBirthday.ModifyUserId = ctx.User.Id;
                    await _birthdayService.UpdateAsync(conn, dbBirthday);

                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Your birthday has been updated!"));
                }
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Error while something bla bla bla");
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("An error occured during execution"));
            }
        }

        [SlashCommand("reset", "Deletes the birthdate and all notifications for it.")]
        public async Task Delete(InteractionContext ctx)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            using var conn = _connectionService.CreateDbConnection();

            try
            {
                var birthday = await _birthdayService.GetAsync(conn, ctx.Guild.Id, ctx.User.Id);

                if (birthday is null)
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("You currently have no birthday set!"));
                    return;
                }

                await _birthdayService.DeleteAsync(conn, birthday);
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Your birthdate has been reset."));
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Error while something bla bla bla");
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("An error occured during execution"));
            }
        }
    }
}
