﻿using BirthdayBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdayBot
{
    internal class BirthdayNotification : IBirthdayNotification
    {
        public int Id { get; set; }
        public ulong NotifyUserId { get; set; }
        public ulong GuildId { get; set; }
        public int BirthdayId { get; set; }
        public int LastNotified { get; set; }
        public BirthdayState State { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public ulong CreateUserId { get; set; }
        public ulong? ModifyUserId { get; set; }
    }
}
