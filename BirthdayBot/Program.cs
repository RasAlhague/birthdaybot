﻿using BirthdayBot;
using BirthdayBot.Service;
using BirthdayBotLib;
using BirthdayBotLib.Commands;
using BirthdayBotLib.Services;
using DSharpPlus;
using DSharpPlus.SlashCommands;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace BirthdayBot
{
    public class Program
    {
        public static void Main()
        {
            MainAsync().GetAwaiter().GetResult();
        }

        static async Task MainAsync()
        {
            BotConfig config = BotConfig.GetInstance();

            var discord = new DiscordClient(new DiscordConfiguration()
            {
                Token = config.BotToken,
                MinimumLogLevel = config.LogLevel,
                TokenType = TokenType.Bot,
                Intents = DiscordIntents.AllUnprivileged
            });

            var slash = discord.UseSlashCommands(new SlashCommandsConfiguration
            {
                Services = new ServiceCollection()
                    .AddSingleton<IBirthdayService<Birthday>, BirthdayService>()
                    .AddSingleton<IDbConnectionService, DbConnectionService>()
                    .AddSingleton<INotificationService<BirthdayNotification>, NotificationService>()
                    .BuildServiceProvider()
            });

            slash.RegisterCommands<BirthdayCommandModule<Birthday, BirthdayNotification>>();
            slash.RegisterCommands<BirthdayNotificationModule<Birthday, BirthdayNotification>>();

            await discord.ConnectAsync();

            var notifier = new BirthdayNotifier<Birthday, BirthdayNotification>(new BirthdayService(), new NotificationService(), new DbConnectionService());
            await notifier.RunNotifierAsync(discord, TimeSpan.FromMinutes(5));

            await Task.Delay(-1);
        }
    }
}

