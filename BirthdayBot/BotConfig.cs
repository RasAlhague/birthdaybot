﻿using dotenv.net;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdayBot
{
    public class BotConfig
    {
        public string BotToken { get; set; }
        public string DbConnString { get; set; }
        public LogLevel LogLevel { get; set; }

        private static BotConfig _instance;

        protected BotConfig()
        {

        }

        public static BotConfig GetInstance()
        {
            if (_instance is null)
            {
                var envVars = DotEnv.Read();

                string botToken = envVars["CHICKENBOT_TOKEN"];
                string dbConnString = envVars["CHICKENBOT_CONN"];
                LogLevel logLevel = Enum.Parse<LogLevel>(envVars["LOG_LEVEL"]);

                _instance = new BotConfig();

                if (!string.IsNullOrEmpty(botToken) && !string.IsNullOrEmpty(dbConnString))
                {
                    _instance.BotToken = botToken;
                    _instance.DbConnString = dbConnString;
                    _instance.LogLevel = logLevel;
                }
                else
                {
                    throw new ArgumentException("Coult not find environment var for bot token and/or conn string!");
                }
            }

            return _instance;
        }
    }
}
