﻿using BirthdayBotLib.Models;
using BirthdayBotLib.Services;
using BirthdayBotLib.Wrappers;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdayBot.Service
{
    internal class NotificationService : INotificationService<BirthdayNotification>
    {
        public async Task<IBirthdayNotification> CreateAsync(IDbConnection conn, BirthdayNotification notification)
        {
            string sql =
                "INSERT INTO BirthdayNotifications " +
                "(NotifyUserId, GuildId, BirthdayId, State, CreateUserId, CreateDate, LastNotified) " +
                "VALUES " +
                "(@notifyUserId, @guildId, @birthdayId, @state, @createUserId, @createDate, @lastNotified); " +
                "SELECT LAST_INSERT_ID()";

            int id = await conn.ExecuteScalarAsync<int>(sql, new
            {
                notifyUserId = notification.NotifyUserId,
                guildId = notification.GuildId,
                birthdayId = notification.BirthdayId,
                state = notification.State,
                createUserId = notification.CreateUserId,
                createDate = notification.CreateDate,
                lastNotified = notification.LastNotified,
            });

            return await GetAsync(conn, id);
        }

        public async Task DeleteAsync(IDbConnection conn, BirthdayNotification notification)
        {
            string sql =
                "DELETE FROM BirthdayNotifications " +
                "WHERE Id = @id";

            await conn.ExecuteAsync(sql, new { id = notification.Id });
        }

        public async Task<IEnumerable<IBirthdayNotification>> GetAllForBirthdayAsync(IDbConnection conn, int birthdayId)
        {
            string sql =
                "SELECT * " +
                "FROM BirthdayNotifications " +
                "WHERE BirthdayId = @id;";

            return await conn.QueryAsync<BirthdayNotification>(sql, new { id = birthdayId });
        }

        public async Task<IEnumerable<IBirthdayNotification>> GetAllForUserAsync(IDbConnection conn, GuildId guildId, DiscordUserId notifyUserId)
        {
            string sql =
                "SELECT * " +
                "FROM BirthdayNotifications " +
                "WHERE GuildId = @guildId " +
                "AND NotifyUserId = @notifyUserId;";

            return await conn.QueryAsync<BirthdayNotification>(sql, new { guildId = guildId.Value, notifyUserId = notifyUserId.Value });
        }

        public async Task<BirthdayNotification> GetAsync(IDbConnection conn, GuildId guildId, DiscordUserId notifyUserId, int birthdayId)
        {
            string sql =
                "SELECT * " +
                "FROM BirthdayNotifications " +
                "WHERE GuildId = @guildId " +
                "AND NotifyUserId = @notifyUserId " +
                "AND BirthdayId = @birthdayId";

            return await conn.QueryFirstOrDefaultAsync<BirthdayNotification>(sql, new
            {
                birthdayId = birthdayId,
                notifyUserId = notifyUserId.Value,
                guildId = guildId.Value
            });
        }

        public async Task<BirthdayNotification> GetAsync(IDbConnection conn, int notificationBirthdayId)
        {
            string sql =
                "SELECT * " +
                "FROM BirthdayNotifications " +
                "WHERE Id = @id;";

            return await conn.QueryFirstOrDefaultAsync<BirthdayNotification>(sql, new { id = notificationBirthdayId });
        }

        public async Task<IBirthdayNotification> UpdateAsync(IDbConnection conn, BirthdayNotification notification)
        {
            string sql =
                "UPDATE BirthdayNotifications " +
                $"SET LastNotified = {DateTime.UtcNow.Year} " +
                "WHERE Id = @id;";

            await conn.ExecuteAsync(sql, new { id = notification.Id });

            return await GetAsync(conn, notification.Id);
        }
    }
}
