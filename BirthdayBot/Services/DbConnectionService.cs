﻿using BirthdayBotLib.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdayBot.Service
{
    internal class DbConnectionService : IDbConnectionService
    {
        public IDbConnection CreateDbConnection()
        {
            return new MySqlConnector.MySqlConnection(BotConfig.GetInstance().DbConnString);
        }
    }
}
