﻿using BirthdayBotLib.Models;
using BirthdayBotLib.Services;
using BirthdayBotLib.Wrappers;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdayBot.Service
{
    internal class BirthdayService : IBirthdayService<Birthday>
    {
        public async Task<Birthday> CreateAsync(IDbConnection conn, GuildId guildId, DiscordUserId birthdayUserId, DateTime birthDate, BirthdayState state)
        {
            string sql =
                "INSERT INTO Birthdays " +
                "(BirthdayUserId, GuildId, Date, State, CreateDate, CreateUserId) " +
                "VALUES " +
                "(@birthdayUserId, @guildId, @birthDate, @state, @createDate, @createUserId);" +
                "SELECT LAST_INSERT_ID()";

            var id = await conn.ExecuteScalarAsync<int>(sql, new
            {
                birthdayUserId = birthdayUserId.Value,
                guildId = guildId.Value,
                birthDate,
                state,
                createDate = DateTime.UtcNow,
                createUserId = birthdayUserId.Value
            });

            return await GetAsync(conn, id);
        }

        public async Task DeleteAsync(IDbConnection conn, Birthday birthdayModel)
        {
            string sql = 
                "DELETE FROM BirthdayNotifications " +
                "WHERE BirthdayId = @id;";

            await conn.ExecuteAsync(sql, new { id = birthdayModel.Id });

            sql =
                "DELETE FROM Birthdays " +
                "WHERE GuildId = @guildId " +
                "AND BirthdayUserId = @userId;";

            await conn.ExecuteAsync(sql, new { guildId = birthdayModel.GuildId, userId = birthdayModel.BirthdayUserId });
        }

        public async Task<Birthday> GetAsync(IDbConnection conn, GuildId guildId, DiscordUserId userId)
        {
            string sql =
                "SELECT * " +
                "FROM Birthdays " +
                "WHERE GuildId = @guildId " +
                "AND BirthdayUserId = @userId;";

            return await conn.QueryFirstOrDefaultAsync<Birthday>(sql, new { guildId = guildId.Value, userId = userId.Value });
        }

        public async Task<Birthday> GetAsync(IDbConnection conn, int birthdayId)
        {
            string sql =
                "SELECT * " +
                "FROM Birthdays " +
                "WHERE Id = @id;";

            return await conn.QueryFirstOrDefaultAsync<Birthday>(sql, new { id = birthdayId });
        }

        public async Task<IEnumerable<Birthday>> GetForGuildAsync(IDbConnection conn, GuildId guildId)
        {
            string sql =
                "SELECT * " +
                "FROM Birthdays " +
                "WHERE GuildId = @guildId;";

            return await conn.QueryAsync<Birthday>(sql, new { guildId = guildId.Value });
        }

        public async Task<Birthday> UpdateAsync(IDbConnection conn, Birthday birthdayModel)
        {
            string sql =
                "UPDATE Birthdays " +
                "SET Date = @birthdate, State = @state, ModifyDate = @modifyDate, ModifyUserId = @modifyUserId " +
                "WHERE Id = @id;";

            await conn.ExecuteAsync(sql, new
            {
                birthdate = birthdayModel.Date,
                state = birthdayModel.State,
                modifyDate = birthdayModel.ModifyDate,
                modifyUserId = birthdayModel.ModifyUserId,
                id = birthdayModel.Id
            });

            return await GetAsync(conn, birthdayModel.Id);
        }
    }
}
